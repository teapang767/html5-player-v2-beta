console.log('file loaded');

var optiShinyPlayer = function (window, document, options){
	'use strict';

	// Does the browser actually support the video element?
	var supportsVideo = !!document.createElement('video').canPlayType;

	if (supportsVideo) {
		// research works best in IE.
		// var loadCSS = function(){
		// 	// set up link
		// 	var cssLocation = '../css/optiShinyPlayer-style.css';
		// 	// var cssLocation = '../css/optiShinyPlayer-style.css';
		// 	var head = document.getElementsByTagName('head')[0];
		// 	var optiShinyPlayerCSSLink = document.createElement('link');
		// 	optiShinyPlayerCSSLink.rel = 'stylesheet';
		// 	optiShinyPlayerCSSLink.href = cssLocation;
		// 	head.appendChild(optiShinyPlayerCSSLink);
		// }

		var loadCSS = function(playerSetupCallBack){
			// set up link
			// var cssLocation = 'http://dev.testing.optimatic.com/html5-player-v2-beta/css/optiShinyPlayer-style.css';
			var cssLocation = '../css/optiShinyPlayer-style.css';
			var head = document.getElementsByTagName('head')[0];
			var optiShinyPlayerCSSLink = document.createElement('link');
			optiShinyPlayerCSSLink.rel = 'stylesheet';
			optiShinyPlayerCSSLink.async = true;

			var xhr = new XMLHttpRequest();
			xhr.open('GET', cssLocation);
			xhr.onload = function(){
				if (xhr.readyState === 4 && xhr.status === 200){
					optiShinyPlayerCSSLink.href = cssLocation;
					head.appendChild(optiShinyPlayerCSSLink);
				} else {
					console.log('it has failed. Returned status of' + xhr.status);
				}
			};
			xhr.send();
		}
		loadCSS();

		// set up player
		var playerSetup = function(options){
			// create DOM elements
			var figureTag = document.createElement('figure');
			figureTag.id = 'videoContainer';
			figureTag['data-fullscreen'] = 'false';

			var videoTag = document.createElement('video');
			videoTag.id = 'video';
			videoTag.preload = 'metadata';
			// fix later to pass options
			videoTag.poster = '../img/silicon-valley-aviato.jpg';
			// set autoplay to false by default.
			videoTag.autoplay = false;

			var sourceTag = document.createElement('source');
			var sourcePlay, sourceType;
			// cycle through the source array.
			for(var x = 0; x < options.source.length; x++){
				// fall thru, .mp4 is best, then rest in order of preference.
				if(options.source[x].substr(-4) === ('.mp4' ||'webm' || '.ogv' || '.m4v')){
					sourcePlay = options.source[x];
					// if webm we go to -4th last, if not, we got to -3 to not pass '.' thru.
					sourceType = 'webm' ? options.source[x].substr(-3) : options.source[x].substr(-4);
				}
			}
			sourceTag.src = sourcePlay;
			sourceTag.type = 'video/' + sourceType;
			sourceTag.id = 'videoTarget';

			var divVideoControls = document.createElement('div');
			divVideoControls.id = 'video-controls';
			divVideoControls.className = 'controls';
			divVideoControls['data-state'] = 'visible';

			var buttonPlayPause = document.createElement('button');
			buttonPlayPause.id = 'playpause';
			buttonPlayPause.type = 'button';
			buttonPlayPause['data-state'] = 'play';
			buttonPlayPause.innerHTML = 'Play/Pause';

			var buttonStop = document.createElement('stop');
			buttonStop.id = 'stop';
			buttonStop.type = 'button';
			buttonStop['data-state'] = 'stop';
			buttonStop.innerHTML = 'Stop';

			var divProgress = document.createElement('div');
			// change to ID #.
			divProgress.className = 'progress';

			var divTimeField = document.createElement('div');
			divTimeField.id = 'timefield';

			var progressTag = document.createElement('progress');
			progressTag.id = 'progress';
			progressTag.value = '0';
			progressTag.min = '0';

			var spanTag = document.createElement('span');
			spanTag.id = 'progress-bar';

			var buttonMute = document.createElement('button');
			buttonMute.id = 'mute';
			buttonMute.type = 'button';
			buttonMute['data-state'] = 'mute';
			buttonMute.innerHTML = 'Mute/Unmute';

			var buttonVolDec = document.createElement('button');
			buttonVolDec.id = 'voldec';
			buttonVolDec.type = 'button';
			buttonVolDec['data-state'] = 'voldec';
			buttonVolDec.innerHTML = 'Vol-';

			var buttonVolInc = document.createElement('button');
			buttonVolInc.id = 'volinc';
			buttonVolInc.type = 'button';
			buttonVolInc['data-state'] = 'volup';
			buttonVolInc.innerHTML = 'Vol+';

			var buttonFS = document.createElement('button');
			buttonFS.id = 'fs';
			buttonFS.type = 'button';
			buttonFS['data-state'] = 'go-fullscreen';
			buttonFS.innerHTML = 'FullScreen';

			var divSBarContainer = document.createElement('div');
			divSBarContainer.id = 'sbar-container';

			var divSBar = document.createElement('div');
			divSBar.id = 'sbar';

			var figCaption = document.createElement('figcaption');

			var anchorTag = document.createElement('a');
			anchorTag.href = 'http://www.optimatic.com/';
			anchorTag.innerHTML = 'Optimatic &copy;';

			// join all DOM elements
			figureTag.appendChild(videoTag);

			// fix when adding options
			videoTag.appendChild(sourceTag);

			figureTag.appendChild(divVideoControls);

			divVideoControls.appendChild(divProgress);
			divProgress.appendChild(progressTag);
			progressTag.appendChild(spanTag);

			divVideoControls.appendChild(buttonPlayPause);
			divVideoControls.appendChild(buttonStop);

			divVideoControls.appendChild(buttonMute);
			divVideoControls.appendChild(buttonVolDec);
			divVideoControls.appendChild(buttonVolInc);			
			divVideoControls.appendChild(divSBarContainer);
			divSBarContainer.appendChild(divSBar);
			divVideoControls.appendChild(divTimeField);

			divVideoControls.appendChild(buttonFS);

			figureTag.appendChild(figCaption);

			// divVideoControls.appendChild(divProgress);
				// divProgress.appendChild(divTimeField);
			// divProgress.appendChild(progressTag);
			// progressTag.appendChild(spanTag);

			// get target element
			var el = document.getElementById('optiShinyPlayer');

			// append element
			el.appendChild(figureTag);

			var aspectRatio = function(width,height){
				// equals to 1.777...
				var sixteenByNine = 16/9;
				// equals to 1.333...
				var fourByThree = 4/3;
				// aspect ratio calculated from setup options
				var optionsAspectRatio = width/height;

				if(optionsAspectRatio === sixteenByNine){
					console.log('equal to 16/9');
				} else if(optionsAspectRatio === fourByThree){
					console.log('equal to 4/3');
				} else {
					console.log('not equal to either 16:9 nor 4:3 aspect ratio');
				}
			 }

			aspectRatio(options.width, options.height);

			// pass options
			figureTag.style.maxWidth = options.width + 'px';
			figureTag.style.maxHeight = options.height + 'px';
			videoTag.poster = options.poster;
			// fix later when adding more sources
			sourceTag.src = options.source[0];
			videoTag.autoplay = options.autoplay;
			// change to work with data-attribute
			divVideoControls.style.display = options.controls ? 'block' : 'none'; 
			videoTag.volume = options.volume;
		};
		playerSetup(options);


		// setup functionality
		// Obtain main elements
		var videoContainer = document.getElementById('videoContainer');
		var video = document.getElementById('video');
		var videoControls = document.getElementById('video-controls');

		// Hide the default controls
		video.controls = false;

		// Display the user defined video controls
		videoControls.setAttribute('data-state', 'visible');

		// Obtain handles to buttons and other elements
		var playpause = document.getElementById('playpause');
		var stop = document.getElementById('stop');
		var mute = document.getElementById('mute');
		var volinc = document.getElementById('volinc');
		var voldec = document.getElementById('voldec');
		var progress = document.getElementById('progress');
		var progressBar = document.getElementById('progress-bar');
		var fullscreen = document.getElementById('fs');
		var sbarContainer = document.getElementById('sbar-container');
		var sbar = document.getElementById('sbar');
		var timefield = document.getElementById('timefield');

		// var playButtonContainer = document.getElementById('playButtonContainer');
		// var playButtonOverlay = document.getElementById('playButtonOverlay');

		// If the browser doesn't support the progress element, set its state for some different styling
		var supportsProgress = (document.createElement('progress').max !== undefined);
		if (!supportsProgress) progress.setAttribute('data-state', 'fake');
		// Check if the browser supports the Fullscreen API
		var fullScreenEnabled = !!(document.fullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled || document.webkitSupportsFullscreen || document.webkitFullscreenEnabled || document.createElement('video').webkitRequestFullScreen);
		
		// If the browser doesn't support the Fulscreen API then hide the fullscreen button
		if (!fullScreenEnabled) {
			fullscreen.style.display = 'none';
		}

		// prevent spacebar from exiting fullscreen when in fullscreen. Just return false to disable its functionality.
		document.addEventListener('keydown', stopSpaceBar, false);
		
		var stopSpaceBar = function(e){
			if(e.keyCode === 32 || e.key === 'Space' || e.which === 32){
				return false;
			}
		}

		var updateSoundBar = function(soundValue){
			sbar.style.width = soundValue * 100+"%";
		}

		var roundToTenth = function(number){
			return Math.round( number * 10 ) / 10;
		}

		var volumeCornerCase = function(v){
			if (video.volume < 0.1){
				video.volume = 0.1;
			}
			if (video.volume > 0.9 && video.volume < 1){
				video.volume = 0.9;
			}
		}

		var volumeControlButton = function(e){
			if(e){
				var percentageSkip = 0.1;
				volumeCornerCase();
				// can raise w/ up, +, = keys or lower volume with down, -, _ keys
				if (e.keyCode === 38 || e.key === "ArrowUp" || e === '+'){
					if (video.volume < 1){
						video.volume = video.volume + percentageSkip;
						video.volume = roundToTenth(video.volume);
						console.log(video.volume);
						updateSoundBar(video.volume);
					}
				}
				if (e.keyCode === 40 || e.key === "ArrowDown" || e === '-'){
					if (video.volume > 0){
						video.volume = video.volume - percentageSkip;
						video.volume = roundToTenth(video.volume);
						console.log(video.volume);
						updateSoundBar(video.volume);
					}
				}
				// if video.volume is equal to zero, change the icon. video.muted is set to true, 
				// then passed to changeButtonState function which then uses the ternary function to change the icon.
				if (video.volume === 0){
					video.muted = true;
				} else video.muted = false;
			}
				changeButtonState('mute');
		}

		//volume adjust with up or down keys
		document.addEventListener('keydown', volumeControlButton, false);

		// change volume with sound bar
		var changeVolume = function(ev) {
			var pos = (ev.pageX  - (this.offsetLeft + this.offsetParent.offsetLeft))/this.offsetWidth;
			video.volume = pos;
			console.log('pos:' + video.volume);
			sbar.style.width = pos * 100 + "%";
		}

		// adjust volume with sound bar
		// function hoisting doesn't work with declartive functions
		sbarContainer.addEventListener('mousedown', changeVolume, false);

		// Change the volume
		var alterVolume = function(dir) {
			volumeControlButton(dir);
		}

		// Set the video container's fullscreen state
		var setFullscreenData = function(state) {
			videoContainer.setAttribute('data-fullscreen', !!state);
			// Set the fullscreen button's 'data-state' which allows the correct button image to be set via CSS
			fullscreen.setAttribute('data-state', !!state ? 'cancel-fullscreen' : 'go-fullscreen');
		}

		// Checks if the document is currently in fullscreen mode
		var isFullScreen = function() {
			return !!(document.fullScreen || document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement || document.fullscreenElement);
		}

		// Fullscreen
		var handleFullscreen = function() {
			// If fullscreen mode is active...	
			if (isFullScreen()) {
					// ...exit fullscreen mode
					// (Note: this can only be called on document)
					if (document.exitFullscreen) document.exitFullscreen();
					else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
					else if (document.webkitCancelFullScreen) document.webkitCancelFullScreen();
					else if (document.msExitFullscreen) document.msExitFullscreen();
					setFullscreenData(false);
				}
				else {
					// ...otherwise enter fullscreen mode
					// (Note: can be called on document, but here the specific element is used as it will also ensure that the element's children, e.g. the custom controls, go fullscreen also)
					if (videoContainer.requestFullscreen) videoContainer.requestFullscreen();
					else if (videoContainer.mozRequestFullScreen) videoContainer.mozRequestFullScreen();
					else if (videoContainer.webkitRequestFullScreen) {
						// Safari 5.1 only allows proper fullscreen on the video element. This also works fine on other WebKit browsers as the following CSS (set in styles.css) hides the default controls that appear again, and 
						// ensures that our custom controls are visible:
						// figure[data-fullscreen=true] video::-webkit-media-controls { display:none !important; }
						// figure[data-fullscreen=true] .controls { z-index:2147483647; }
						video.webkitRequestFullScreen();
					}
					else if (videoContainer.msRequestFullscreen) videoContainer.msRequestFullscreen();
					setFullscreenData(true);
				}
			}

		var playPauseVideo = function(e){
			if (video.paused || video.ended) {
				video.play();
			} else{
				video.pause();
			}
			console.log('trigger');
		}

		// Only add the events if addEventListener is supported (IE8 and less don't support it, but that will use Flash anyway)
		if (document.addEventListener) {
			// Wait for the video's meta data to be loaded, then set the progress bar's max value to the duration of the video
			video.addEventListener('loadedmetadata', function() {
				progress.setAttribute('max', video.duration);
				timefield.innerHTML = formattedTimeField();
				updateSoundBar(video.volume);
			});

			// Changes the button state of certain button's so the correct visuals can be displayed with CSS
			var changeButtonState = function(type) {				
				// Play/Pause button
				if (type == 'playpause') {
					if (video.paused || video.ended) {
						playpause.setAttribute('data-state', 'play');
					}
					else {
						playpause.setAttribute('data-state', 'pause');
					}
				}
				// Mute button
				if (type == 'mute') {
					mute.setAttribute('data-state', video.muted ? 'unmute' : 'mute');
					var isMute = document.getElementById("mute").attributes['data-state'];
					if (video.muted){
						sbar.style.display = 'none';
					} else {
						sbar.style.display = 'block';
					}
				}
			}

			// Add event listeners for video specific events
			// playButtonContainer.addEventListener('click', playPauseVideo, false);

			// ability to play or pause a video when the video is clicked.
			video.addEventListener('click', playPauseVideo,false);

			video.addEventListener('click', function(){
				changeButtonState('playpause');
			}, false);
			video.addEventListener('play', function() {
				changeButtonState('playpause');
			}, false);
			video.addEventListener('pause', function() {
				changeButtonState('playpause');
			}, false);
			video.addEventListener('volumechange', function() {
				volumeControlButton();
			}, false);

			// Add events for all buttons			
			playpause.addEventListener('click', playPauseVideo);		

			//Add key event for play/pause
			document.addEventListener('keydown', function(e) {
				if (e.keyCode === 80) {
					if (video.paused || video.ended) {
						playPauseVideo();
					}
					else {
						playPauseVideo();
					}	
				}
			},false);			

			// The Media API has no 'stop()' function, so pause the video and reset its time and the progress bar
			// reload video- fn doesn't hoist for some reason.
			var stopVideo = function(){
				if (video.play){
					video.pause();
					video.currentTime = 0;
					progress.value = 0;
				}
				// Update the play/pause button's 'data-state' which allows the correct button image to be set via CSS
				changeButtonState('playpause');
			}
			// functionality to stop btn.
			stop.addEventListener('click', stopVideo);

			mute.addEventListener('click', function(e) {
				video.muted = !video.muted;
				changeButtonState('mute');
			});
			volinc.addEventListener('click', function(e) {
				alterVolume('+');
			});
			voldec.addEventListener('click', function(e) {
				alterVolume('-');
			});
			fs.addEventListener('click', function(e) {
				handleFullscreen();
			});
			//Add Key Event for Fullscreen
			document.addEventListener('keydown', function(e) {
				if (e.keyCode === 13) {
					handleFullscreen();
				}
			});

			//As the video is playing, update the timefield
			var formattedTimeField = function() {
				var seconds = Math.round(video.currentTime);
				var minutes = Math.floor(video.currentTime/60);
				if (minutes > 0) seconds -= minutes*60;
				if (seconds.toString().length === 1) seconds = '0' + seconds;  

				var totalSeconds = Math.round(video.duration);
				var totalMinutes = Math.floor(video.duration/60);
				if(totalMinutes > 0) totalSeconds -= totalMinutes*60;
				if(totalSeconds.toString().length === 1) totalSeconds = '0' + totalSeconds;

				return minutes + ":" + seconds + "/" + totalMinutes + ":" + totalSeconds;
			}

			// As the video is playing, update the progress bar
			video.addEventListener('timeupdate', function() {
				// For mobile browsers, ensure that the progress element's max attribute is set
				if (!progress.getAttribute('max')) progress.setAttribute('max', video.duration);
				progress.value = video.currentTime;
				progressBar.style.width = Math.floor((video.currentTime / video.duration) * 100) + '%';
				timefield.innerHTML = formattedTimeField();
			});

			// React to the user clicking within the progress bar
			progress.addEventListener('click', function(e) {
				//var pos = (e.pageX  - this.offsetLeft) / this.offsetWidth; // Also need to take the parent into account here as .controls now has position:relative
				var pos = (e.pageX  - (this.offsetLeft + this.offsetParent.offsetLeft)) / this.offsetWidth;
				video.currentTime = pos * video.duration;
			});

			//Add Key Event for Progress Track Forward/Back
			document.addEventListener('keydown', function(e) {
				if (e.keyCode === 37 || e.keyCode === "LeftArrow") {
					video.currentTime = video.currentTime - 5;
				}
				else if (e.keyCode === 39  || e.keyCode === "RightArrow") {
					video.currentTime = video.currentTime + 5;
				}
			});

			// Listen for fullscreen change events (from other controls, e.g. right clicking on the video itself)
			document.addEventListener('fullscreenchange', function(e) {
				setFullscreenData(!!(document.fullScreen || document.fullscreenElement));
			});
			document.addEventListener('webkitfullscreenchange', function() {
				setFullscreenData(!!document.webkitIsFullScreen);
			});
			document.addEventListener('mozfullscreenchange', function() {
				setFullscreenData(!!document.mozFullScreen);
			});
			document.addEventListener('msfullscreenchange', function() {
				setFullscreenData(!!document.msFullscreenElement);
			});
		}

		// Public API methods
		// Adjust video volume
		var volumeAdjust = function(vol){
			video.volume = vol;
			updateSoundBar(video.volume);
			if (video.volume === 0){
				video.muted = true;
			} else video.muted = false;
			changeButtonState('mute');	
		}

		var reloadVideoAPI= function(){
			stopVideo();
		}

		var removePlayer = function(){
			var el_ = document.getElementById('optiShinyPlayer');
			el_.innerHTML = '';
		}

		var reloadPlayer = function(){
			playerSetup();
		}

	 }

	return {
		// Public API
		VERSION: '0.0.1',
		init: function(){
			console.log('init fired');
		},
		setup: function(options){
			console.log('setup fired');
		},
		pause: function(){
			console.log('pause fired');
			video.pause();
		},
		play: function(){
			console.log('play fired');
			video.play();
		},
		volume: function(vol){
			volumeAdjust(vol);
		},
		stop: function(){
			console.log('reload fired');
			reloadVideoAPI();
		},
		remove: function(){
			removePlayer();
		},
		reload: function(){
			this.remove();
			reloadPlayer();
		}
	}
 };














